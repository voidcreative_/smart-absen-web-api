<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id_siswa');
            $table->integer('nis')->unsigned();
            $table->string('nama_siswa');
            $table->string('ttl');
            $table->integer('id_kelas')->unsigned();
            $table->string('password')->unique();
            $table->text('alamat');
            $table->string('no_hp')->unique();
            $table->string('jk');
            $table->integer('id_jurusan')->unsigned();
            $table->string('image');
            $table->timestamps();
            $table->foreign('id_kelas')->references('id_kelas')->on('kelas')->onDelete('cascade');
            $table->foreign('id_jurusan')->references('id_jurusan')->on('jurusans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
